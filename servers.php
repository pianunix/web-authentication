<?php
session_start();
$link = mysqli_connect("localhost", "root", "mwn2019", "authlogin");
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
// Attempt select query execution
//$sql = "SELECT username,hostname,clpanel,linkcp FROM servers where class='1' by id";
$sql = "SELECT *  FROM servers where userid=".$_SESSION["userid"]." ";
?>
</table>
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <title>WHM & SSH Authentication by Masterweb</title>
    <link href="http://monitoring.localdomain/autologin/css/dashboard.css" rel="stylesheet" />
    <script src="http://monitoring.localdomain/autologin/css/dashboard.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="./assets/js/require.min.js"></script>
</head>

<body>


    <div class="header py-4">
        <div class="container">
            <div class="d-flex">
                <a class="header-brand" href="#">
                    WHM & SSH Authentication by Masterweb <?php echo $_SESSION['userid']; ?>
                </a>
                <div class="d-flex order-lg-2 ml-auto">
                    <div class="nav-item d-none d-md-flex">
                        <a href="https://github.com/tabler/tabler" class="btn btn-sm btn-outline-primary"
                            target="_blank">Document</a>
                    </div>
                    <div class="dropdown">
                        <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                            <span class="avatar" style="background-image: url(./demo/faces/female/25.jpg)"></span>
                            <span class="ml-2 d-none d-lg-block">
                                <span class="text-default"></span>
                                <small class="text-muted d-block mt-1">Administrator</small>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="#">
                                <i class="dropdown-icon fe fe-log-out"></i> Sign out
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
                    data-target="#headerMenuCollapse">
                    <span class="header-toggler-icon"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 ml-auto">
                    <form class="input-icon my-3 my-lg-0">
                        <input type="search" class="form-control header-search" placeholder="Search&hellip;"
                            tabindex="1">
                        <div class="input-icon-addon">
                            <i class="fe fe-search"></i>
                        </div>
                    </form>
                </div>
                <div class="col-lg order-lg-first">
                    <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                        <li class="nav-item">
                            <a href="servers.php" class="nav-link active">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="addservers.php" class="nav-link">Add Servers</a>
                        </li>
                        <li class="nav-item">
                            <a href="addusers.php" class="nav-link">Add Users</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="my-3 my-md-5">
        <div class="container">
            <div class="page-header">
                <h1 class="page-title">
                    Welcome
                </h1>
            </div>

            <p>From here you can login to cPanel/WHM !!</p>
            <div class="card">
                <div class="table-responsive">
                    <?php
                if($result = mysqli_query($link, $sql)){
                    if(mysqli_num_rows($result) > 0){
                ?>
                    <table class="table card-table table-vcenter text-nowrap">
                        <tbody>
                            <?php while($row = mysqli_fetch_array($result)) { ?>
                            <tr>
                                <td>
                                    <span class="avatar"
                                        style="background-image: url(./demo/faces/female/25.jpg)"></span>
                                </td>
                                <td>
                                    <div>username</div>
                                    <div class="small text-muted">
                                        <?php echo $row['username']; ?>
                                    </div>
                                </td>
                                <td>
                                    <div>hostname</div>
                                    <div class="small text-muted">
                                        <?php echo $row['hostname']; ?>
                                    </div>
                                </td>
                                <td>
                                    <div>ip address</div>
                                    <div class="small text-muted">
                                        <?php echo $row['ipaddr']; ?>
                                    </div>
                                </td>
                                <td>
                                    <div>Control Panel</div>
                                    <div class="small text-muted">
                                        <?php echo $row['clpanel']; ?>
                                    </div>
                                </td>
                                <td>
                                    <div>action</div>
                                    <a href="<?php echo $row['linkcp']; ?>" target="_blank">
                                        <div class="btn btn-info"> cPanel & WHM login</div>
                                    </a>
                                    <a href="http://login.maintenis.com/webconsole.php" target="_blank">
                                        <div class="btn btn-info">SSH Login</div>
                                    </a>
                                    <a href="delservers2.php" _blank">
                                        <div class="btn btn-danger">Delete</div>
                                    </a>
                                </td>
                                <?php } ?>
                    </table>
                    <?php 
                    }
                }
                ?>
                </div>
            </div>


            <a href="logout.php" class="btn btn-danger">Sign Out of Your Account</a>

        </div>

    </div>


</body>

</html>
